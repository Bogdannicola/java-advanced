package sda.example;

public interface Instrument {
    void setGama(Gama game);
    void playNote(Note note);
}
