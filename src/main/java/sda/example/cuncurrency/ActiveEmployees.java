package sda.example.cuncurrency;

import java.time.LocalTime;
import java.util.LinkedList;
import java.util.Queue;

public class ActiveEmployees {

    Queue<Employee> activeEmployees = new LinkedList<>();

    public synchronized void employeeArrives(String name) {
        Employee newEmployee = new Employee(name);
        activeEmployees.add(newEmployee);
        newEmployee.start();
    }

    public synchronized void employeeLeaves() {
        Employee leavingE = activeEmployees.poll();
        System.out.println(leavingE.getEmployeeName() + " it's time to go home " + Employee.dtf.format(LocalTime.now()));
        for(Employee current : activeEmployees) {
            current.speedUp();
        }
        leavingE.interrupt();
    }

    public synchronized boolean areEmployeesAtWork() {
        return ! activeEmployees.isEmpty();
    }

}
