package sda.example.cuncurrency;

public class Bench {
    private int nrSeatsAvailable;

    public Bench(int nrSeatsAvailableAtTheBeginning) {
        nrSeatsAvailable = nrSeatsAvailableAtTheBeginning;
    }

    public synchronized void takeASeat() {
        if (nrSeatsAvailable > 0) {
            System.out.println("Take a seat - " + Thread.currentThread().getName());
            nrSeatsAvailable--;
            System.out.println("Seats available: " + nrSeatsAvailable + " - " + Thread.currentThread().getName());
        } else {
            System.out.println("No seats available  - " + Thread.currentThread().getName() + "!");
        }
    }

}
