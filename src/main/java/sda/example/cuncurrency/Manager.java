package sda.example.cuncurrency;

public class Manager extends Thread {

    private final ActiveEmployees atWork;

    public Manager() {
        atWork = new ActiveEmployees();
        atWork.employeeArrives("Bob");
        atWork.employeeArrives("Ghita");
        atWork.employeeArrives("Ana");
        atWork.employeeArrives("Maria");
        atWork.employeeArrives("Pali");
    }

    @Override
    public void run() {
        while (atWork.areEmployeesAtWork() == true) {

            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            atWork.employeeLeaves();
        }
    }
}
