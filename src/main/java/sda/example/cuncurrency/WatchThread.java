package sda.example.cuncurrency;

public class WatchThread implements Runnable {

    @Override
    public void run() {
        for (int i=0;i<5;i++) {
            System.out.println("Thread: " + Thread.currentThread().getName() + " execution #" + i);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                System.out.println("Exception while thread is sleeping: " + e.getMessage());
            }
        }
    }

}
