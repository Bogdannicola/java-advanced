package sda.example.cuncurrency;

import java.util.Date;

public class ThreadExampleMain {

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Main thread: " + Thread.currentThread().getName());

        System.out.println("Current time before sleep: " + new Date());

        Thread.sleep(500);

        System.out.println("Current time after sleep: " + new Date());

        SleepThread sThread = new SleepThread();
        sThread.start();

        Thread watchThread1 = new Thread(new WatchThread());
        Thread watchThread2 = new Thread(new WatchThread());

        watchThread1.setPriority(10);

        watchThread1.start();
        watchThread2.start();

        System.out.println("End: " + new Date());

        System.out.println("-----------------------------");

        Bench bench = new Bench(1);
        Thread seatterThread1 = new Thread(new SeatterThread(bench));
        Thread seatterThread2 = new Thread(new SeatterThread(bench));
        Thread seatterThread3 = new Thread(new SeatterThread(bench));
        Thread seatterThread4 = new Thread(new SeatterThread(bench));

        seatterThread1.start();
        seatterThread2.start();
        seatterThread3.start();
        seatterThread4.start();

    }

}
