package sda.example.cuncurrency;

import lombok.Data;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

@Data

public class Employee extends Thread {

    final private String employeeName;
    private int workingSpeed = 10; // in seconds
    public static final DateTimeFormatter dtf = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);

    public Employee(String employeeName) {
        this.employeeName = employeeName;
    }

    @Override
    public void run() {

        LocalTime timeToWork = LocalTime.now();
        System.out.println(employeeName + " : I came to work at " + dtf.format(timeToWork));

        boolean working = true;

        while (working == true) {
            try {
                Thread.sleep(workingSpeed * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                working = false;
            }
            System.out.println(employeeName + " : I am still working!");

        }
    }

    public synchronized void speedUp() {
        workingSpeed = workingSpeed -2; // in seconds
    }
}
