package sda.example.cuncurrency;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SeatterThread implements Runnable {
    private final Bench bench;

    @Override
    public void run() {
        bench.takeASeat();
    }

}
