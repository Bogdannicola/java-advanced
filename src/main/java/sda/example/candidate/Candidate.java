package sda.example.candidate;

import sda.example.department.DepartmentName;

import java.security.PublicKey;

public class Candidate {
    private String name;
    private Integer age;
    private String address;
    private String emailAddress;
    private int levelOfCompetence;
    private int yearsOfExperience;
    private DepartmentName departmentName;
    private CandidateStatus candidateStatus;

    public Candidate(String name,
                     Integer age,
                     String address,
                     String emailAddress,
                     int levelOfCompetence,
                     int yearsOfExperience,
                     DepartmentName departmentName) {
        this.name = name;
        this.age = age;
        this.address = address;
        this.emailAddress = emailAddress;
        this.levelOfCompetence = levelOfCompetence;
        this.yearsOfExperience = yearsOfExperience;
        this.departmentName = departmentName;
        this.candidateStatus = CandidateStatus.AWAITING;

    }

    public Candidate() {}

    public String getName() {
        return name;
    }

    public void setName(String numeleCandidatului) {
        this.name = numeleCandidatului;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public int getLevelOfCompetence() {
        return levelOfCompetence;
    }

    public void setLevelOfCompetence(int levelOfCompetence) {
        this.levelOfCompetence = levelOfCompetence;
    }

    public int getYearsOfExperience() {
        return yearsOfExperience;
    }

    public void setYearsOfExperience(int yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

    public DepartmentName getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(DepartmentName departmentName) {
        this.departmentName = departmentName;
    }

    public CandidateStatus getCandidateStatus() {
        return candidateStatus;
    }

    public void setCandidateStatus(CandidateStatus candidateStatus) {
        this.candidateStatus = candidateStatus;
    }

    public String toString() {
        return "Candidate: " + name + " has " + yearsOfExperience + " years of experience," + " level of competence "
                + levelOfCompetence + " applied for " + departmentName + " and has the status " + candidateStatus + "\n";
    }

    //    public String getName() {
//       return this.name;
//    }
//
//    public void setName(String numeleCandidatului) {
//        this.name = numeleCandidatului;
//    }
//
//    public CandidateStatus getCandidateStatus() {
//
//        return candidateStatus;
//    }
//
//    public void setCandidateStatus() {
//
//        this.candidateStatus = candidateStatus;
//    }
//
//    public Integer getAge() {
//        return this.age;
//    }
//
//    public void  setAge(Integer age) {
//        this.age = age;
//    }
//
//    public String getAddress() {
//        return this.address;
//    }
//
//    public void setAddress() {
//        this.address = address;
//    }
//
//    public int getLevelOfCompetence() {
//        return this.levelOfCompetence;
//    }
//    public void setLevelOfCompetence() {
//        this.levelOfCompetence = levelOfCompetence;
//    }
//
//    public int getYearsOfExperience() {
//        return this.yearsOfExperience;
//    }
//
//    public void setYearsOfExperience() {
//        this.yearsOfExperience = yearsOfExperience;
//    }
//
//    public String getEmailAddress() {
//        return this.emailAddress;
//    }
//
//    public void setEmailAddress() {
//        this.emailAddress = emailAddress;
//    }
}
