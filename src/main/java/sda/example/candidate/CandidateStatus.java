package sda.example.candidate;

public enum CandidateStatus {
    ACCEPTED,
    REJECTED,
    AWAITING
}
