package sda.example.collections;

import java.util.*;

public class SetExamples {

    public static void main(String[] args) {
        // citesc de la tastatura actiune executam: add = add element; delete = delete element; print = print list
        Scanner scanner = new Scanner(System.in);

        Set<String> colors = new TreeSet<>();

        Action action = null;
        do {
            action = getAction(scanner);
            switch (action) {
                case ADD:
                    // citim elementul
                    System.out.print("Color to add: ");
                    String newItemToAdd = scanner.nextLine();
                    colors.add(newItemToAdd);
                    break;
                case DELETE:
                    // citim elementul
                    System.out.print("Color to delete: ");
                    String newItemToDelete = scanner.nextLine();
                    colors.remove(newItemToDelete);
                    break;
                case PRINT:
                    System.out.print("Colors: ");
                    for (String item : colors) {
                        System.out.print(item + ", ");
                    }
//                    Iterator<String> it = colors.iterator();
//                    while(it.hasNext()) {
//                        System.out.print(it.next() + ", ");
//                    }
                    System.out.println();
                    break;
            }
        } while (action != Action.EXIT);

        List<String> colorsList = new ArrayList<String>(colors);
        Collections.sort(colorsList);
    }

    private static Action getAction(Scanner scanner) {
        Action action = null;
        do {
            System.out.print("command: ");
            String command = scanner.nextLine();
            try {
                action = Action.valueOf(command.toUpperCase());
            } catch (IllegalArgumentException ex) {
                System.out.println("Please set a valid command!");
            }
        } while (action == null);

        return action;
    }

}
