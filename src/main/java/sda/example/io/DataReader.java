package sda.example.io;

import java.util.List;

public interface DataReader {

    public List<String> getAllStrings(String source);

    public void storePerson(Person p);
    public Person getPerson(String cnp);

    public int countStrings(String source);

}
