package sda.example.io;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Person implements Serializable {

    private final String cnp;
    private final String name;
    private final int age;

}
