package sda.example.io;

import java.io.File;
import java.util.List;

public class DataReaderMain {
    public static void main(String[] args) {
        DataReader dataReader = new FileDataReader();

        List<String> allStrings = dataReader.getAllStrings("C:\\Users\\const\\IdeaProjects\\SDA\\src\\main\\resources" + File.separator + "fileWithStrings.txt");

        System.out.println("All strings that we read from the file:");
        for (String s: allStrings) {
            System.out.println(s);
        }

        Person p1 = new Person("123456", "Popescu Marin", 25);
        Person p2 = new Person("121342", "Pop Ionela", 26);

//        dataReader.storePerson(p1);
//        dataReader.storePerson(p2);

        Person personFromFile1 = dataReader.getPerson("123456");
        Person personFromFile2 = dataReader.getPerson("121342");

        System.out.println("Persons equals: " + p1.equals(personFromFile1));

        int allStringsCount = dataReader.countStrings("textdump" + File.separator + "textFile_1.txt");
        System.out.println("Number of words: " + allStringsCount);

    }
}
