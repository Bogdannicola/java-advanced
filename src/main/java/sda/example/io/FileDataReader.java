package sda.example.io;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileDataReader implements DataReader {

    private static final String PREFIX = "persons" + File.separator;
    private static final String EXTENSION = ".txt";

    public List<String> getAllStrings(String source) {
        List<String> result = new ArrayList<String>();

        File file = new File(source);
        FileReader fr = null;
        BufferedReader br = null;
        // try (BufferedReader br2 = new BufferedReader(new FileReader(file))) { // alternative method
        try {
            fr = new FileReader(file);
            br = new BufferedReader(fr);
            String newLine = br.readLine();
            while (null != newLine) {
                result.addAll(getAllStringsFromLine(newLine));

                newLine = br.readLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println("Fisierul <" + source + "> nu a fost gasit! ");
        } catch (IOException e) {
            System.out.println("Eroare la citirea din fisierul <" + source + ">. Ex: " + e.getMessage());
        } finally {
            try {
                if (null != br) {
                    br.close();
                }
                if (null != fr) {
                    fr.close();
                }
            } catch (IOException e) {
            }
        }

        return result;
    }

    @Override
    public void storePerson(Person p) {
        File pFile = new File(buildFileName(p.getCnp()));

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(pFile))){
            oos.writeObject(p);
            System.out.println("Person saved in file: " + p);
        } catch (IOException e) {
            System.out.println("Exception while writing person to file! Ex: " + e.getMessage());
        }
    }

    @Override
    public Person getPerson(String cnp) {
        File personFile = new File(buildFileName(cnp));

        Person person = null;

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(personFile))) {
            person = (Person)ois.readObject();
            System.out.println("Person read from file: " + person);
        } catch (Exception e) {
            System.out.println("Exception while reading person to file! Ex: " + e.getMessage());
        }
        return person;
    }

    @Override
    public int countStrings(String source) {
        List<String> allLines = new ArrayList<>();
        try {
            allLines = Files.readAllLines(Paths.get(source));
        } catch (IOException e) {
            System.out.println("Exception while reading from file: " + source + ". Ex: " + e.getMessage());
        }

        int allWordsCount = 0;

        for (String s: allLines) {
            String[] allWordsInLine = s.split(" ");
            allWordsCount= allWordsCount + allWordsInLine.length;
        }

        return allWordsCount;
    }

    private String buildFileName(String cnp) {
        File parentFolder = new File(PREFIX);
        if (!parentFolder.exists()) {
            parentFolder.mkdirs();
        }
        return PREFIX + cnp + EXTENSION;
    }

    private List<String> getAllStringsFromLine(String newLine) {

        String[] allStrings = newLine.split(" ");

        // Method 1:
//        List<String> result = new ArrayList<String>();
//
//        for (String oneString : allStrings) {
//            result.add(oneString);
//        }

        // Method 2:
        List<String> result = Arrays.asList(allStrings);

        return result;
    }

}
