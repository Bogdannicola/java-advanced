package sda.example.department;

import sda.example.candidate.Candidate;

public abstract class Department {
    protected DepartmentName name;
    protected int minLevelOfCompetence;


    public abstract void evaluate(Candidate candidate);
}
