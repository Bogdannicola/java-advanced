package sda.example;

public class Trompet implements Instrument {

    private Gama gama;

    @Override
    public void setGama(Gama gama) {
        this.gama = gama;
    }

    @Override
    public void playNote(Note note) {
        System.out.println("Trompet-" + gama + "-" + note);
    }

}
