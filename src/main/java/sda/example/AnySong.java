package sda.example;

public abstract class AnySong implements Song {

    private int songLenght;
    private String name;

    protected abstract Note[] getNotes();
    protected abstract Gama getGama();

    AnySong(String name, int songLenght) {
        this.songLenght = songLenght;
        this.name = name;
    }

    @Override
    public int getLenght() {
        return songLenght;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void play(Instrument instrument) {
        instrument.setGama(getGama());
        for (Note note: getNotes()) {
            instrument.playNote(note);
        }
    }

}
