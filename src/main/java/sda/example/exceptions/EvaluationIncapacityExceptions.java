package sda.example.exceptions;

public class EvaluationIncapacityExceptions extends RuntimeException {

    public EvaluationIncapacityExceptions() {
        super("This candidate didn't apply for a valid department!");
    }

}
