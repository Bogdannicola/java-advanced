package sda.example.recruitment;

import sda.example.candidate.Candidate;

public interface Evaluator {

    void evaluate(Candidate candidate);
}
