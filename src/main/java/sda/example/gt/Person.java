package sda.example.gt;

public class Person implements Comparable<Person>{

    private String name;
    private int age;
    private int height;

    public Person(String name, int age, int height) {
        this.name = name;
        this.age = age;
        this.height = height;
    }

    @Override
    public int compareTo(Person o) {
        return this.height - o.height;
    }

    public String getName() {
        return name;
    }

}
