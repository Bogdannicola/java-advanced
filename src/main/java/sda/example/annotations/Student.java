package sda.example.annotations;

import lombok.*;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student { // POJO

    private String cnp;
    private String stundetName;
    private String className;
    private Date birthDate;

    /**
     *
     * @param dateAsString
     * @deprecated Please use setBirthDate(birthDate: Date)
     */
    @Deprecated
    public void setBirthDate(String dateAsString) {
        // ...
    }

}
