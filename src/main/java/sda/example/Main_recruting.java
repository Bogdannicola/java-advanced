package sda.example;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import sda.example.candidate.Candidate;
import sda.example.department.Department;
import sda.example.department.DepartmentName;
import sda.example.department.Marketing;
import sda.example.department.Production;
import sda.example.recruitment.Company;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main_recruting {
    public static void main(String[] args) throws IOException {

        List<Candidate> candidates = readCandidatesToJsonFile();

        Company company = new Company("SDA Recruiting", candidates);
        company.recruiting();

        writeCandidatesToTxtFile(candidates);
    }

    public static void writeCandidatesToTxtFile(List<Candidate> candidates) {

        try (BufferedWriter writer = new BufferedWriter(new FileWriter("candidati-acceptati.txt"))) {
            writer.write(candidates.toString());
        } catch (IOException e) {
            System.out.println("Could not write to file.");
        }


    }

    public static void writeCandidatesToJsonFile(List<Candidate> candidates) throws IOException {
        ObjectMapper objectMapper= new ObjectMapper();
        objectMapper.writeValue(new File("candidati-acceptati.json"), candidates);
    }

    public static List<Candidate> readCandidatesToJsonFile() throws IOException {
        ObjectMapper objectMapper= new ObjectMapper();
        List<Candidate> candidates = objectMapper.readValue(new File("candidati-initiali.json"), new TypeReference<List<Candidate>>(){});

        return candidates;
    }
}
