//package sda.example;
//
//public class Main1 {
//    public static void main(String[] args) {
//
////        Shape s1 = new Shape(UnitOfMeasure.CM, 10, 10);
//        Shape s1 = new Rectangle(UnitOfMeasure.CM, 10, 10);
//        System.out.println("s1: " + s1);
//​
//        ((Rectangle)s1).printRetable();
//​
////        Shape s2 = new Shape(UnitOfMeasure.MM, 100, 100);
//        Shape s2 = new Rectangle(UnitOfMeasure.MM, 100, 100);
//        System.out.println("s2: " + s2);
//​
//        System.out.println("s1 == s2: " + (s1 == s2));
//        System.out.println("s1.equals(s2): " + s1.equals(s2));
//​
//        s1.setUm(UnitOfMeasure.MM);
//        System.out.println("s1: " + s1);
//        System.out.println("s1 == s2: " + (s1 == s2));
//        System.out.println("s1.equals(s2): " + s1.equals(s2));
//​
//        System.out.println("// -----------------------------------------------------//");
//​
//        Rectangle r1 = new Rectangle();
//​
//        Rectangle r2 = new Rectangle(100, 10);
//​
//        Shape c1 = new Circle(5);
//​
//        // ((Rectangle)c1).printRetable();
//​
//        System.out.println("// -----------------------------------------------------//");
//​
//        for (UnitOfMeasure um: UnitOfMeasure.values()) {
//            System.out.println("UM: " + um.name() + "->" + um);
//        }
//​
//        UnitOfMeasure newUM = UnitOfMeasure.valueOf("MM");
//​
//        System.out.println("newUM == UnitOfMeasure.MM: " + (newUM == UnitOfMeasure.MM));
//​
//        System.out.println("// -----------------------------------------------------//");
//​
//        Rectangle a = new Rectangle(UnitOfMeasure.CM, 1, 2);
//        Shape b = new Circle(UnitOfMeasure.CM, 3);
//​
//        System.out.println("Area a: " + a.getArea());
//        System.out.println("Area b: " + b.getArea());
//        System.out.println("Perimeter a: " + a.getPerimeter());
//        System.out.println("Perimeter b: " + b.getPerimeter());
//​
//        a.printRetable();
//​
//    }
//
//}
