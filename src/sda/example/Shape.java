package sda.example;

import java.util.Objects;

public abstract class Shape {

    private final static UnitOfMeasure UM_DEFAULT = UnitOfMeasure.MM;

    private UnitOfMeasure um = UM_DEFAULT;
    private ShapeProjection projection;

    public Shape(UnitOfMeasure um, int width, int length) {
        this.um = um;
        projection = new ShapeProjection(width, length);
        System.out.println("Shape created #1: " + super.toString());
    }

    public Shape(int width, int length){
        this(UM_DEFAULT, width, length);
        System.out.println("Shape created #2: " + super.toString());
    }

    public Shape(){
        super();
        projection = new ShapeProjection();
        System.out.println("Shape created #3: " + super.toString());
    }

    public abstract double getArea();
    public abstract double getPerimeter();

    public void setUm(UnitOfMeasure newUm) {
        if (!(this.um == newUm)) {
            projection.setWidth(um.getDimensionInNewUM(newUm, projection.getWidth()));
            projection.setLength(um.getDimensionInNewUM(newUm, projection.getLength()));
            this.um = newUm;
        }
    }

    public UnitOfMeasure getUm(){
        return um;
    }

    public ShapeProjection getProjection() {
        return projection;
    }

    @Override
    public String toString() {
        return "Shape{" +
                "um=" + um +
                ", projection=" + projection +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shape shape = (Shape) o;
        return  um.getDimensionInNewUM(shape.um, projection.getWidth()) == shape.projection.getWidth() &&
                um.getDimensionInNewUM(shape.um, projection.getLength()) == shape.projection.getLength();
    }

    @Override
    public int hashCode() {
        return Objects.hash(um.getDimensionInNewUM(UnitOfMeasure.MM, projection.getLength()), um.getDimensionInNewUM(UnitOfMeasure.MM, projection.getLength()));
    }

}
