package sda.example;

public class Rectangle extends Shape {

    public int width;
    public int length;

    public Rectangle() {
        System.out.println("Rectangle created #1: " + this);
    }

    public Rectangle(int width, int length) {
        super(width, length);

        this.width = width;
        this.length = length;
        System.out.println("Rectangle created #2: " + this);
    }

    public Rectangle(UnitOfMeasure um, int width, int length) {
        super(um, width, length);

        this.width = width;
        this.length = length;
        System.out.println("Rectangle created #3: " + this);
    }

    @Override
    public double getArea() {
        return width * length;
    }

    @Override
    public double getPerimeter() {
        return 2 * (width + length);
    }

    public void printRetable() {
        int widthInMM = super.getUm().getDimensionInNewUM(UnitOfMeasure.MM, width);
        int lengthInMM = super.getUm().getDimensionInNewUM(UnitOfMeasure.MM, length);
        for (int j=0;j<widthInMM;j++) {
            for (int i = 0; i < lengthInMM; i++) {
                System.out.print("-");
            }
            System.out.println();
        }
    }

}
