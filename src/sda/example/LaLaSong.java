package sda.example;

public class LaLaSong extends AnySong implements Song {

    private static Note[] notes = new Note[]{Note.DO_MAJOR, Note.MI, Note.SOL, Note.SOL, Note.RE};

    private Gama gama;

    public LaLaSong(Gama gama) {
        super(LaLaSong.class.getSimpleName(), notes.length);
        this.gama = gama;
    }

    @Override
    public Gama getGama() {
        return gama;
    }

    @Override
    public Note[] getNotes() {
        return notes;
    }

}
