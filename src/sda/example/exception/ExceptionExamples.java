package sda.example.exception;

import java.util.Scanner;

public class ExceptionExamples {

    public static void main(String[] args) {
        System.out.print("Please enter the number: ");
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        scanner.close();

        NumberPrinter np = new NumberPrinterWithExceptions();
        np.printNumber(line);
    }

}
