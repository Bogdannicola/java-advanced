package sda.example;

public class Circle extends Shape {

    private int r;

    public Circle(int r) {
        super(2*r, 2*r);
        this.r = r;
        System.out.println("Circle created #1: " + this);
    }

    public Circle(UnitOfMeasure um, int r) {
        super(um, 2*r, 2*r);
        this.r = r;
        System.out.println("Circle created #2: " + this);
    }

    @Override
    public double getArea() {
        return Math.PI * r * r;
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * r;
    }

}
