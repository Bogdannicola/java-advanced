package sda.example;

public enum Note {
    DO_MINOR, RE, MI, FA, SOL, LA, SI, DO_MAJOR
}
