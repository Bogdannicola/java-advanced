package sda.example.collections;

import java.util.*;

public class MapExamples {

    public static void main(String[] args) {

        Map<Integer, Car> cars = new HashMap<>();
        cars.put(1, new Car());

        Car car2 = new Car();
        car2.setModel("Pontiac");
        car2.setYear(1997);
        car2.addProperties("color", "black");
        car2.addProperties("last owner", "Bob");
        cars.put(2, car2);

        printCars(cars);

        cars.get(1).addProperties("color", "white");
        cars.get(1).addProperties("aspect", "very good");

        printCars(cars);

    }

    private static void printCars(Map<Integer, Car> cars) {
        for (Map.Entry<Integer, Car> idToCarEntry : cars.entrySet()) {
            Integer carId = idToCarEntry.getKey();
            Car car = idToCarEntry.getValue();
            System.out.print(carId + ": \n {\n");
            System.out.print("\tmodel: " + car.getModel() + "\n");
            System.out.print("\tyear: " + car.getYear() + "\n");
            System.out.print("\tproperties: {\n");
            for (Map.Entry<String, String> propertyNameToValue: car.getProperties().entrySet()) {
                System.out.print("\t\t" + propertyNameToValue.getKey() + ": " + propertyNameToValue.getValue() + "\n");
            }
            System.out.print("\n\t}\n");
            System.out.print("\n}\n");
        }
    }
}
